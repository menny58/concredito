const mysql = require('mysql');

const conection = {
    host: 'localhost',
    user: 'root',
    password: '5894manuel',
    database: 'concredito'
}

/**
 * @param {string} sql - Consulta a ejecutar
 * @param callback - Accion a ejecutar despues de ejecutar la consulta
 * @param params - parámetos de la consulta. Deben de referenciar en el mismo orden
 *      dentro de la consulta como signos de interrogacion (?)
 */
exports.query = (sql, callback, params) =>{
    // console.log(params);
    let cn = mysql.createConnection(conection);
    cn.connect((err) =>{
        if(err) throw err;
        cn.query(sql, params, (err, result) =>{
            if(err){
                console.log(`sp -- ${sql} params ${params}`);
                throw err;
            }
            callback(JSON.parse(JSON.stringify(result)));
            cn.destroy;
        });
    })
}


/**
     * Ejecuta una consulta en MySQL 
     * @param {string} sql - Consulta a ejecutar. 
     * @param {Object[]} params - Parámetros de la consulta. Se deben referenciar en el mismo orden
     *      dentro de la consulta (sql) como signos de interrogación (?).
     * @returns Promise<MySqlResult>
*/
exports.queryAsync = async (sql, params) =>{
    return new Promise((resolve, rej) =>{
        let cn = mysql.createConnection(conection)
        cn.connect((err) => {
            if(err) throw err;
            cn.query(sql, params, (err, result) =>{
                if(err)  throw err;
                resolve(result);
                cn.destroy;
            });
        });
    });
}