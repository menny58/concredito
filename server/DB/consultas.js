const con = require('./conections');

exports.save = async (data) =>{
    let params = [data.nombre, data.apellidoP, data.apellidoM, data.calle, data.numero, data.colonia, data.cp, data.telefono, data.rfc];
    return await con.queryAsync("CALL SaveFile(?,?,?,?,?,?,?,?,?)", params);
}

exports.saveFile = async(data) =>{
    // console.log("entra");
    let params = [data.type, data.fileName, data.data, data.userName, data.apellidoP, data.apellidoM, data.calle, data.numero, data.colonia, data.cp, data.telefono, data.rfc];
    return await con.queryAsync("CALL Save(?,?,?,?,?,?,?,?,?,?,?,?)", params);
}

exports.list = async (estatus) =>{
    return new Promise((resolve, reject) => {
        con.query("CALL GetList(?)", (result) =>{
            if(result[0].lenght === 0) resolve(result[0][0]);
            // console.log(result[0]);
            resolve(result[0]);
        },estatus);
    });
}

exports.evaluar = async () => {
    return new Promise((resolve, reject) => {
        con.query("CALL GetEvaluar()", (result) =>{
            if(result[0].lenght === 0) resolve(result[0][0]);
            // console.log(result[0]);
            resolve(result[0]);
        })
    })
}

exports.cambiarEstatus = async(data) => {
    let params = [data.id, data.coment, data.accion]
    return await con.queryAsync("CALL UpadateStatus(?,?,?)", params)
}

exports.prospect = async(id) =>{
    let params = [parseInt(id)];
    return new Promise((resolve, reject) => {
        con.query("CALL GetProspecto(?)", (result) =>{
            if(result[0] === 0) resolve(result[0][0])
            resolve(result[0]);
        }, params);
    });
} 